--
-- Copyright © 2016-2021 The CPS Authors
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

CREATE DATABASE cps
CREATE RETENTION POLICY one_day ON cps DURATION 1d REPLICATION 2 SHARD DURATION 1h
CREATE RETENTION POLICY one_week ON cps DURATION 1w REPLICATION 2 SHARD DURATION 1d DEFAULT
CREATE RETENTION POLICY two_weeks ON cps DURATION 2w REPLICATION 2 SHARD DURATION 1d
CREATE RETENTION POLICY one_month ON cps DURATION 30d REPLICATION 2 SHARD DURATION 1d
CREATE RETENTION POLICY two_months ON cps DURATION 60d REPLICATION 2 SHARD DURATION 1d
CREATE RETENTION POLICY three_months ON cps DURATION 90d REPLICATION 2 SHARD DURATION 1d
CREATE RETENTION POLICY six_months ON cps DURATION 180d REPLICATION 2 SHARD DURATION 1d
CREATE RETENTION POLICY one_year ON cps DURATION 365d REPLICATION 2 SHARD DURATION 1d

-- ///////////////////////////////////////////////////////////////////////////////////////////
-- This retention policy is used for storing data of ts_kv_latest_cf measurement only       //
-- ///////////////////////////////////////////////////////////////////////////////////////////
CREATE RETENTION POLICY for_ever ON cps DURATION 0s REPLICATION 2 SHARD DURATION 1d

USE cps
-- ///////////////////////////////////////////////////////////////////////////////////////////
-- insert data to measurement ts_kv_latest_cf of given RP (disabled by default)             //
-- corresponding SELECT clause as following:                                                //
--                                                                                          //
-- SELECT entity_id,entity_type,"key",bool_v,str_v,long_v,dbl_v,ts                          //
-- FROM "for_ever"."ts_kv_latest_cf"                                                        //
--   WHERE entity_type='TENANT'                                                             //
--   AND entity_id='00000000-0000-0000-0000-000000000000'                                   //
--   AND "key"='empty'                                                                      //
--                                                                                          //
-- ///////////////////////////////////////////////////////////////////////////////////////////
INSERT INTO for_ever ts_kv_latest_cf,entity_id=00000000-0000-0000-0000-000000000000,entity_type=TENANT,key=empty bool_v=true,str_v="",long_v=0i,dbl_v=0.0,ts=0i 1626313487160000001
