/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package driver

import (
	"fmt"
	"github.com/beevik/etree"
	"github.com/use-go/onvif"
	"github.com/use-go/onvif/media"
	"io/ioutil"
	"net/http"
	"strings"
)

type FunctionType string

const (
	StreamUri         FunctionType = "StreamUri"
	SystemDateAndTime FunctionType = "SystemDateAndTime"
)

type Client struct {
	device *onvif.Device
	config Config
}

func NewClient(config Config) (*Client, error) {
	device, err := NewDevice(config)
	if err != nil {
		return nil, err
	}
	return &Client{
		config: config,
		device: device,
	}, nil
}

// NewDevice return new onvif device.
func NewDevice(config Config) (*onvif.Device, error) {
	device, err := onvif.NewDevice(config.URL)
	if err != nil {
		return nil, err
	}
	device.Authenticate(config.UserName, config.Password)
	return device, nil
}

// StreamURL return the video stream address url.
func (c *Client) StreamURL() (string, error) {
	su := media.GetStreamUri{}
	resp, err := c.device.CallMethod(su)
	if err != nil {
		return "", err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("stream url error: %s", err)
	}
	return c.ParseXML(string(body), "./Body/GetStreamUriResponse/MediaUri/Uri")
}

// ParseXML return xml parse body.
func (c *Client) ParseXML(body, xpath string) (string, error) {
	doc := etree.NewDocument()
	if err := doc.ReadFromString(body); err != nil {
		return "", err
	}
	// XML Parse
	resp := doc.Root().FindElement(xpath)
	if resp == nil {
		return "", fmt.Errorf("find element %s parse xml error", xpath)
	}
	return strings.Split(resp.Text(), "")[0], nil
}
