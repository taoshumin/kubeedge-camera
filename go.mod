module x6t.io/camera

go 1.16

require (
	github.com/beevik/etree v1.1.0
	github.com/sailorvii/goav v0.1.4
	github.com/use-go/onvif v0.0.1
	golift.io/ffmpeg v1.0.1
)
