#!/bin/bash

# CREATE DATABASE cps
# CREATE RETENTION POLICY one_day ON cps DURATION 1d REPLICATION 2 SHARD DURATION 1h
# CREATE RETENTION POLICY one_week ON cps DURATION 1w REPLICATION 2 SHARD DURATION 1d DEFAULT
# CREATE RETENTION POLICY two_weeks ON cps DURATION 2w REPLICATION 2 SHARD DURATION 1d
# CREATE RETENTION POLICY one_month ON cps DURATION 30d REPLICATION 2 SHARD DURATION 1d
# CREATE RETENTION POLICY two_months ON cps DURATION 60d REPLICATION 2 SHARD DURATION 1d
# CREATE RETENTION POLICY three_months ON cps DURATION 90d REPLICATION 2 SHARD DURATION 1d
# CREATE RETENTION POLICY six_months ON cps DURATION 180d REPLICATION 2 SHARD DURATION 1d
# CREATE RETENTION POLICY one_year ON cps DURATION 365d REPLICATION 2 SHARD DURATION 1d
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE DATABASE cps"
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE RETENTION POLICY one_day ON cps DURATION 1d REPLICATION 2 SHARD DURATION 1h"
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE RETENTION POLICY one_week ON cps DURATION 1w REPLICATION 2 SHARD DURATION 1d DEFAULT"
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE RETENTION POLICY two_weeks ON cps DURATION 2w REPLICATION 2 SHARD DURATION 1d"
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE RETENTION POLICY one_month ON cps DURATION 30d REPLICATION 2 SHARD DURATION 1d"
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE RETENTION POLICY two_months ON cps DURATION 60d REPLICATION 2 SHARD DURATION 1d"
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE RETENTION POLICY three_months ON cps DURATION 90d REPLICATION 2 SHARD DURATION 1d"
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE RETENTION POLICY six_months ON cps DURATION 180d REPLICATION 2 SHARD DURATION 1d"
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE RETENTION POLICY one_year ON cps DURATION 365d REPLICATION 2 SHARD DURATION 1d"
# CREATE RETENTION POLICY for_ever ON cps DURATION 0s REPLICATION 2 SHARD DURATION 1d
curl -i -XPOST http://127.0.0.1:8086/query --data-urlencode "q=CREATE RETENTION POLICY for_ever ON cps DURATION 0s REPLICATION 2 SHARD DURATION 1d"
# INSERT INTO for_ever ts_kv_latest_cf,entity_id=00000000-0000-0000-0000-000000000000,entity_type=TENANT,key=empty bool_v=true,str_v="",long_v=0i,dbl_v=0.0,ts=0i 1626313487160000001
curl -i -XPOST 'http://127.0.0.1:8086/write?db=cps&rp=for_ever' --data-binary 'ts_kv_latest_cf,entity_id=00000000-0000-0000-0000-000000000000,entity_type=TENANT,key=empty bool_v=true,str_v="",long_v=0i,dbl_v=0.0,ts=0i 1626313487160000001'

# INSERT MANY DATA
curl -i -XPOST 'http://localhost:8086/write?db=cps' --data-binary 'cpu_load_short,host=server02 value=0.67
cpu_load_short,host=server02,region=us-west value=0.55 1422568543702900257
cpu_load_short,direction=in,host=server01,region=us-west value=2.0 1422568543702900257'