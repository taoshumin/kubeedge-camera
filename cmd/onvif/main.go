/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"log"
	"x6t.io/camera/driver"
)

func main() {
	client,err:=driver.NewClient(driver.Config{
		URL:      "192.168.10.64:80",
		UserName: "admin",
		Password: "admin123456",
	})
	if err!=nil{
		log.Fatalf("new onvif client error: %s",err)
	}
	url,err:=client.StreamURL()
	if err!=nil{
		log.Fatalf("stream url error: %s",err)
	}
	fmt.Println(url)
}
