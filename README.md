# Kubeedge-Camera

主要用于边缘端摄像头设备采集测试。


## FFmpeg Install

```shell
sudo apt-get -y install autoconf automake build-essential libass-dev libfreetype6-dev libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texi2html zlib1g-dev
sudo apt install -y libavdevice-dev libavfilter-dev libswscale-dev libavcodec-dev libavformat-dev libswresample-dev libavutil-dev
sudo apt-get install yasm
export FFMPEG_ROOT=$HOME/ffmpeg
export CGO_LDFLAGS="-L$FFMPEG_ROOT/lib/ -lavcodec -lavformat -lavutil -lswscale -lswresample -lavdevice -lavfilter"
export CGO_CFLAGS="-I$FFMPEG_ROOT/include"
export LD_LIBRARY_PATH=$HOME/ffmpeg/lib
```

# Quick start

- goav >= 0.1.4
- FFmpeg >= 4.4


# 获取拉流地址

`协议://用户名:密码@摄像头ip:默认端口/频道`

```shell
rtsp://admin:admin123456@192.168.10.47:554/h264/ch1/main/av_stream
```

